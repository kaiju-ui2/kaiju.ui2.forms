import FieldStore from "FieldGroups/store";
import {action, computed, observable} from "mobx";
import arrayMove from 'array-move';


const behaviorSetValue = (store, value, ValueStore) => {
    switch (store.conf.behavior) {
        case "list": {
            return (Array.isArray(value)? value : []).map((val) => {
                return new ValueStore(val, store.conf, store)
            });
        }
        case "range": {
            return {
                min: new ValueStore((value && value.constructor === Object && value.min) || store.emptyValue(), store.conf, store),
                max: new ValueStore((value && value.constructor === Object && value.max) || store.emptyValue(), store.conf, store),
            };
        }
        default: {
            return new ValueStore(value || store.emptyValue(), store.conf, store);
        }
    }
};


const behaviorIsChanged = (store) => {
    switch (store.conf.behavior) {
        case "list":
            return store.listHasChange;

        case "range":
            return !(store.oldValue.min.equal(store.value.min) && store.oldValue.max.equal(store.value.max));

        default:
            return !store.oldValue.equal(store.value)
    }
};


export default class BehaviorFieldStore extends FieldStore {
    @observable newElement;

    constructor(conf, formStore) {
        super(conf, formStore);
        this.newElement = new this.ValueStore(this.emptyValue(), this.conf, this)
    }

    @action setValue(value) {
        return behaviorSetValue(this, value, this.ValueStore)
    }

    @computed get isChanged() {
        return behaviorIsChanged(this)
    }
    saveValue() {
        if (this.isChanged) {
            this.saveValueInCache(this.cleanData)
        } else {
            this.clearCache()
        }

    }

    emptyValue() {
        return ""
    }

    @computed get listHasChange() {
        if (!this.value && !this.oldValue) return false;
        if (this.value && !this.oldValue) return true;
        if (!this.value && this.oldValue) return true;

        if (Array.isArray(this.value) && Array(this.oldValue) && this.value.length === this.oldValue.length) {
            const c = this.value.filter((newValue, index) => {
                return !this.oldValue[index].equal(newValue)
            });
            return c.length !== 0
        }

        return true
    }

    @action add() {
        if (this.newElement.value && Array.isArray(this.value) && !this.newElement.hasError()) {
            this.value.push(this.newElement);
            this.newElement = new this.ValueStore(this.emptyValue(), this.conf, this)
        }
    }

    @action onSortEnd = ({oldIndex, newIndex}) => {
        if (Array.isArray(this.value)) {
            this.value = arrayMove(this.value, oldIndex, newIndex)
            this.saveValue()

        }
    };

    get cleanData() {
        switch (this.conf.behavior) {
            case "list":
                return Array.isArray(this.value) ? this.value.map((val)=>(val.cleanData)) : [];

            case "range":
                return {
                    min: this.value.min.cleanData,
                    max: this.value.max.cleanData
                };

            default:
                return this.value.cleanData
        }
    }
}
