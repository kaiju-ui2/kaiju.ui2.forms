import React from "react";
import {ListBehaviorComponent, RangeBehaviorComponent} from "Behavior/index";
import Label from "FieldGroups/Label";
import ErrorMessageComponent from "FieldGroups/ErrorMessageComponent";
import {uuid4} from "@kaiju.ui2/components/src/utils";


export default class BehaviorComponent extends React.Component {
    field;

    getBehavior() {
        switch (this.props.store.conf.behavior) {
            case "list":
                return <ListBehaviorComponent key={uuid4()} store={this.props.store} component={this.field}/>;
            case "range":
                return <RangeBehaviorComponent key={uuid4()} store={this.props.store} component={this.field}/>;
            default:
                return <this.field
                    {...this.props.store}
                />
        }
    }


    render() {
        return (
            <div className="form-group mw-500 relative pb-3" key={uuid4()}>
                <Label {...{props: this.props}}/>
                {this.getBehavior()}
                <ErrorMessageComponent  key={uuid4()} {...this.props}/>
            </div>
        )
    }
}
