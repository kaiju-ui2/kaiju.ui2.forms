import React from 'react';
import {observer} from "mobx-react";
import {SortableContainer, SortableElement, sortableHandle} from 'react-sortable-hoc';
import {uuid4} from "@kaiju.ui2/components/src/utils";


// Отвечает за формат работы c value

@observer
class ListBehaviorComponent extends React.Component {
    constructor(props) {
        super(props)
        this.Component = this.props.component;
    }

    AddNewElement = observer(() => {
        const Component = this.Component;

        return (
            <div className="pt-3" style={{position: "relative"}}>
                <label className="fs-12">{utils.getTranslation("Field.new_row")}</label>
                <Component
                    {...this.props.store}
                    value={this.props.store.newElement}
                    key={uuid4()}
                >
                    <i
                        style={{
                            position: "absolute",
                            right: "-35px",
                            fontSize: "23px",
                            paddingTop: "10px",
                            color: "grey"
                        }}
                        className="icon-plus pointer"
                        onClick={e => this.props.store.add()}
                    />
                </Component>
            </div>
        )
    });

    SortableItem = SortableElement(({value, sortIndex}) => {
        const Component = this.Component;
        const DragHandle = this.DragHandle;
        return (
            <div className="col-12" style={{position: "relative"}} key={uuid4()}>
                <DragHandle key={uuid4()}/>
                <div style={{position: "relative"}} key={uuid4()} className="mr-auto">
                    <Component {...this.props.store} value={value} key={uuid4()}>
                        <i
                            style={{
                                position: "absolute",
                                right: "-35px",
                                fontSize: "23px",
                                paddingTop: "10px",
                                color: "grey"
                            }}
                            className="icon-cross pointer"
                            onClick={(e) => {
                                e.stopPropagation();
                                this.props.store.removeElement(sortIndex)
                            }}
                        />
                    </Component>
                </div>
                {this.props.store.value.length - 1 === sortIndex &&
                <div className="border-bottom"/>
                }
            </div>)
    });

    SortableList = SortableContainer(observer(() => {
        const SortableItem = this.SortableItem;
        return (
            <div className="row">
                {this.props.store.value.map((value, index) => {
                    return (
                        <SortableItem key={uuid4()} sortIndex={index} index={index} value={value}/>
                    )
                })}
            </div>
        );
    }));

    DragHandle = sortableHandle(() => <i className="icon-arrows draggable" style={{
        position: "absolute",
        left: "-18px",
        top: "12px",
        fontSize: "18px",
        cursor: "move"
    }}/>);

    render() {
        return (
            <div className="pl-5">
                <this.SortableList onSortEnd={this.props.store.onSortEnd} helperClass='sortableHelper' lockAxis={'y'} useDragHandle/>
                <this.AddNewElement/>
            </div>
        )
    }
}

class RangeBehaviorComponent extends React.Component {
    render() {
        return (
            <div className="pl-4">
                <label className="fs-12">{utils.getTranslation("Field.min_value")}</label>
                <this.props.component {...this.props.store} value={this.props.store.value.min}
                                      key={uuid4()}/>

                <label className="fs-12">{utils.getTranslation("Field.max_value")}</label>
                <this.props.component {...this.props.store} value={this.props.store.value.max}
                                      key={uuid4()}/>
            </div>
        )
    }
}

export {ListBehaviorComponent, RangeBehaviorComponent}
