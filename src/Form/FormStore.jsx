import {action, computed, observable} from "mobx";
import {FieldsKeyMap, FieldsKindMap} from "FieldGroups";
import axios from "axios";
import getCache, {clearCache} from "./cache";
import {uuid4} from "@kaiju.ui2/components/src/utils";

class FormGroupStore {
    constructor({key, missing_count, fields, label}) {
        this.uuid = uuid4();
        this.key = key;
        this.missing_count = missing_count;
        this.fields = fields;
        this.label = label
    }

    @computed get showed() {
        if (this.fields.length > 0) {
            const showed = this.fields.filter((store) => {
                return store.show
            });
            return showed.length > 0
        }
        return false
    }
}


export class FormActionsStore {
    @observable isFetching;
    @observable dataInitialized = false;

    constructor(store) {
        this.store = store;

        this.load()
    }

    @action initData(data) {
        clearCache(this.store);
        const groups = [];
        const fieldStores = {};

        data.map((group) => {
            let groupFieldStores = [];
            group.label = group.label && group.label.length > 0 ? group.label : utils.getTranslation(group.key);

            group.fields.map((field) => {
                let {kind, key} = field;

                let fieldStore = this.store.fieldsKeyMap[key] || this.store.fieldsKindMap[kind];

                if (fieldStore) {
                    let store = new fieldStore(field, this.store);
                    fieldStores[key] = store;
                    groupFieldStores.push(store)
                } else {
                    console.warn(`Field with kind="${kind}" and key="${key}" not registered`)
                }
            });

            groups.push(new FormGroupStore({
                ...group,
                fields: groupFieldStores
            }))
        });

        this.store.groups.map((g, i) => delete this.store.groups[i]);
        Object.keys(this.store.fieldStores).map(k => delete this.store.fieldStores[k]);


        this.store.groups = groups;
        this.store.fieldStores = fieldStores;


        this.dataInitialized = true
    }

    @action.bound load() {
        this.isFetching = true;

        axios.post('/public/rpc',
            this.store.conf.requests.load
        ).then(response => {

            if (response.data.result) {
                let result = response.data.result;
                this.initData(result.fields || result)
                return
            }

            return Promise.reject(response);
        })
            .catch(response => {

                if (this.store.conf.requests.load.errorCallback) {
                    this.store.conf.requests.load.errorCallback(response.data)
                } else {
                    utils.handleNotifyExceptions(response)
                }

            }).finally(() => {
            this.isFetching = false;
        })
    }

    /**
     *  Обновить состояние формы с бекенда.
     */
    @action.bound reload() {
        this.store.all = true;
        this.store.uuid = uuid4();
        this.load()
    }

    @action showChanged() {
        this.store.all = false;
        Object.values(this.store.fieldStores).map((store) => {
            if (!store.isChanged) {
                store.toggleShow()

            }
        })
    }

    @action showAll() {
        this.store.all = true;
        Object.values(this.store.fieldStores).map((store) => {
            if (!store.show) {
                store.toggleShow()
            }
        })
    }

    /**
     * Используется для получения актуальных данных из кэша.
     * Нужно для переключения форм на странице карточки товара.
     *
     */
    reCreateValue() {
        Object.values(this.store.fieldStores).map((store) => {
            store.reCreateValue()
        })
    }

}


export class FormStore {
    @observable groups = []; // для отрисовки
    @observable fieldStores = {}; // для филдСторов
    @observable all = true;
    @observable key;

    constructor(conf, extraParams) {
        this.uuid = uuid4();
        this.key = conf.key || this.uuid;
        this.disableGroup = conf.disableGroup !== false; // by default - True
        this.conf = conf;
        this.extraParams = extraParams || conf.extraParams || {} // будут переданы все fieldConf-гам
        this.prefix = this.conf.prefix;
        this.requests = conf.requests;
        this.hasHeader = conf.hasHeader === true; // by default - False

        // TODO: сделать опцию conf.readOnly - только читать форму

        // groupIds . contains
        // missing True . equal


        this.fieldsKindMap = {
            ...FieldsKindMap,
            ...this.conf.fieldsKindMap
        };

        this.fieldsKeyMap = {
            ...FieldsKeyMap,
            ...this.conf.fieldsKeyMap
        };

        this.actions = new FormActionsStore(this);

        window.onbeforeunload = this.beforeReload.bind(this);
        this.cache = getCache(this);

        if (this.conf.clearCache) {
            this.cache.clear()
        }

        this.errorCallback = conf.errorCallback // для вызова asyncErrorStore

    }

    @action.bound setErrors(errors) {
        Object.keys(errors).map(key => {
            if (this.fieldStores[key]) {
                this.fieldStores[key].setErrors(errors[key])
            }
        })
    }

    get cleanData() {
        const respData = {};
        Object.values(this.fieldStores).map((store) => {
            if (store.isChanged) {
                respData[store.conf.key] = store.cleanData !== undefined ? store.cleanData : null;
            }
        });
        return respData
    }

    beforeReload(event) {
        return Object.keys(this.cleanData).length > 0 ? utils.getTranslation("Message.lost_changes") : null
    }

    validate() {
    }

    @computed get changes() {
        const result = Object.values(this.fieldStores).filter((store) => {
            return store.isChanged
        });
        return result.length
    }

    hasKeyChanged(key) {
        return this.fieldStores[key] ? this.fieldStores[key].isChanged : false;
    }

    @action.bound setSaveKeys(keys) {
        keys.map(key => {
            this.fieldStores[key].setSaveValue()
        })
    }

}