import {action, autorun, computed, observable} from "mobx";
import Axios from "axios";
import {notifySuccess} from "@kaiju.ui2/components/src/notifications";
import {FormStore} from "./FormStore";

/**
 * Стор для осуществления работы с одной формой.
 * Используется для страниц редактирования и добавления
 */
class EditFormStore {
    @observable isFetching = false;
    @observable isChanged = false;

    /**
     *
     * @param conf основной конфиг
     * @var labelKey - ключ для доступа к fieldStores, чаще всего "id" или "key"
     * @var formStore
     * @var successCallback  - callback после сохранения формы
     */
    constructor(conf) {
        this.conf = conf;
        this.labelKey = conf.labelKey;
        this.idKey = conf.idKey || "id";
        this.successCallback = this.conf.successCallback;
        this.conf.formStore.clearCache = true;

        this.formStore = new FormStore(this.conf.formStore, this.conf.extraParams || {});
        this.changesCallback = this.conf.changesCallback;
        this.saveRequestConf = (this.conf.requests || {}).save || this.conf.formStore.requests.save;

        autorun(() => {
            if (!this.formStore) {
                return
            }
            let isChanged = this.formStore.changes > 0;

            if (this.isChanged === isChanged) {
                return
            }

            this.isChanged = isChanged;

            if (this.changesCallback) {
                this.changesCallback(this.isChanged)
            }
        })
    }


    @action
    reloadForms() {
        this.formStore.actions.reload();
    }

    setErrors(response) {
        let {error} = response.data;

        if (error && error.data.type === "ModelValidationException") {
            this.formStore.setErrors(error.data.fields)
        } else {
            utils.handleNotifyExceptions(response)
        }
    }

    /**
     * Смотрит произошел ли init двух форм
     * @returns {boolean}
     */
    @computed get dataInitialized() {
        return this.formStore.actions.dataInitialized
    }

    /**
     * Лейбл для хедера
     *
     * @returns {string|""} - актуальный лейбл на момент редактирования
     */
    @computed get label() {

        if (this.formStore.fieldStores &&
            this.formStore.fieldStores[this.labelKey] &&
            this.formStore.fieldStores[this.labelKey].value.value
        ) {
            return this.formStore.fieldStores[this.labelKey].value.value
        }

        if (this.formStore.fieldStores &&
            this.formStore.fieldStores[this.idKey] &&
            this.formStore.fieldStores[this.idKey].value.value) {
            return `[${this.formStore.fieldStores[this.idKey].value.value}]`
        }


        return ""
    }

    saveCallback() {
        const isValid = true; // TODO:

        if (!isValid) {
            // TODO:
        }

        let params = {
            ...(this.saveRequestConf.params || {}),
            ...this.formStore.cleanData
        };

        this.isFetching = true;

        Axios.post('/public/rpc', {
                method: this.saveRequestConf.method,
                params: params
            }
        ).then((response) => {
            const result = response.data.result;

            if (result) {
                notifySuccess();

                if (this.successCallback) {
                    this.successCallback(result);
                }

                this.reloadForms();
            } else {
                this.setErrors(response)
            }

        })
            .catch((response) => {
                if (this.saveRequestConf.errorCallback) {
                    this.saveRequestConf.errorCallback(response.data)
                } else {
                    utils.handleNotifyExceptions(response)
                }
            })
            .finally(() => this.isFetching = false)
    }
}

export default EditFormStore