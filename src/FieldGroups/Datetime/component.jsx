import React from "react";
import BehaviorComponent from "Behavior/component";
import {observer} from "mobx-react";
import ErrorComponent from "Error";
import flatpickr from "flatpickr";
import 'flatpickr/dist/flatpickr.css'
import "flatpickr/dist/l10n/ru.js"
import BaseStorage from "@kaiju.ui2/components/src/stores/storage";
import {uuid4} from "@kaiju.ui2/components/src/utils";

class SimpleDatetimeComponent extends React.Component {
    valueRef;
    picker;

    constructor(props){
        super(props);
        this.value = props.value;

        this.disabled = props.conf.disabled;

        this.conf = {
            ...props.conf,
            readonly: props.conf.is_default || props.conf.is_system
        };
        this.ref = React.createRef()
    }

    renderValueError = observer(() => {
        if (this.valueRef && this.value.valueError){
            this.valueRef.classList.add("form-error")
        } else if (this.valueRef){
            this.valueRef.classList.remove("form-error")
        }

        return (
            <div>
                {this.value.valueError &&
                <ErrorComponent id={uuid4()} value={this.value.valueError}/>
                }
            </div>
        )
    });

    changeValue(event){
        this.value.valueOnChange(this.ref.current.value);
    }

    componentDidMount() {
        this.changeValueBind = this.changeValue.bind(this);

        this.ref.current.addEventListener("change", this.changeValueBind);
        let locale =BaseStorage.getItem("locale");

        if (locale) {
            locale = locale.split("_")[0]
        }     else {
            locale = "ru"
        }

        this.picker = flatpickr(this.ref.current, {
            "enableTime": this.value.conf.enable_time,
            "dateFormat": this.value.conf.enable_time? "Y-m-d H:i:S": "Y-m-d",
            "locale": locale,
            "minDate": this.value.conf.min_value,
            "maxDate": this.value.conf.max_value
        });
    }

    componentWillUnmount() {
        this.ref.current.removeEventListener("change", this.changeValueBind);
        this.picker.destroy()
    }

    render() {
        return (
            <div className="form-group mw-500 relative" style={{display: "flex", flexWrap: "wrap"}}>
                <div
                    className=" mw-500 col-12 pr-0 pl-0"
                    ref={ref => this.valueRef = ref}
                    data-for={this.errorId}
                    data-tip=""
                >
                    <input
                        disabled={this.disabled}
                        type="text"
                        ref={this.ref}
                        defaultValue={this.value.value}
                        className="form-control m-input"
                    />
                    <this.renderValueError/>
                </div>

                {this.props.children}
            </div>
        )
    }
}


export default class DatetimeComponent extends BehaviorComponent {
    field = SimpleDatetimeComponent;
}
