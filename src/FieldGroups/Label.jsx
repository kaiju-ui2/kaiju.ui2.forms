import {observer} from "mobx-react";
import React from "react";
import ReactTooltip from 'react-tooltip'
import 'FieldGroups/style.scss'
import {uuid4} from "@kaiju.ui2/components/src/utils";

const getLabel = (conf) => {
    return conf.label && conf.label.length > 0 ? conf.label : utils.getTranslation(`Field.${conf.key}`)
};


const Label = observer(({id, props}) => {
    const dataFor = uuid4();
    return (
        <React.Fragment>
            <label htmlFor={id} className="checkbox-label mr-auto"
                   style={{color: props.store.isChanged ? "#36a3f7" : "rgb(111, 114, 127)"}}>
                {getLabel(props.store.conf)}

                {props.store.isChanged &&
                <React.Fragment>
                    <i data-tip={utils.getTranslation("Message.is_changed")} className="icon-warn ml-3" style={{
                        position: "relative",
                        top: "3px",
                        color: "orange"
                    }} data-for={dataFor}/>
                    <ReactTooltip
                        type={"warning"}
                        effect={"solid"}
                        id={dataFor}
                    />
                </React.Fragment>
                }
            </label>
            <div className="labelInfo">
                {props.store.conf.per_channel &&
                    <span>{utils.getTranslation("Field.per_channel")} </span>
                }

            </div>
        </React.Fragment>
    )
});

export default Label
