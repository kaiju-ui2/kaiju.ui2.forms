import NumberComponent from "FieldGroups/Number/component";
import {action, observable} from "mobx";
import BehaviorFieldStore from "Behavior/behaviorUtils";
import validate from "validate.js";


class ValueStore {
    @observable value;
    @observable valueError;

    timeoutValidate;

    constructor(value, conf, store) {
        this.value = value || conf.default || "";
        this.conf = conf;
        this.store = store
    }

    @action validate() {
        if (isNaN(this.value) && this.value !== undefined) return `${utils.getTranslation("ValidationError.invalid")}`;
        if (!this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) return "";
        if (this.conf.required && (this.value === "" || this.value === null || this.value === undefined)) {
            return `^${utils.getTranslation("ValidationError.Required")}`
        }
        const greaterThan = this.conf.min_value || 0;
        const lessThanOrEqualTo = this.conf.max_value;
        const is_negative = this.conf.is_negative || false;

        const constraints = {
            value: {
                numericality: {
                    onlyInteger: !this.conf.decimal_value,

                    greaterThanOrEqualTo: !is_negative || !this.conf.min_value ? greaterThan : undefined,
                    lessThanOrEqualTo: (this.conf.max_value !== undefined || this.conf.max_value !== "" || this.conf.max_value !== null) ? lessThanOrEqualTo : undefined,

                    notGreaterThanOrEqualTo: `^${utils.getTranslation("ValidationError.LessThan")} ${greaterThan}`,
                    notLessThanOrEqualTo: `^${utils.getTranslation("ValidationError.GreatThan")} ${lessThanOrEqualTo}`,
                    notInteger: `^${utils.getTranslation("ValidationError.IntegerOnly")}`
                }
            }
        };

        const result = validate.validate({value: this.value}, constraints);

        this.valueError = result ? result.value.join("<br/> ") : ""

    }

    callValidate() {
        if (this.timeoutValidate) {
            clearTimeout(this.timeoutValidate)
        }

        this.timeoutValidate = setTimeout(() => {
            this.validate()
        }, 500)
    }

    @action valueOnChange(value) {
        this.value = value ? Number(value) : value;
        this.callValidate()
    }

    equal(valueStore) {
        if (!this.value && !valueStore.value) return true;
        return this.value === valueStore.value
    }
    hasError() {
        return !!this.valueError
    }
    get cleanData(){
        return this.value
    }

}


export default class NumberStore extends BehaviorFieldStore {
    Component = NumberComponent;

    init() {
        this.ValueStore = ValueStore;
    }

}


class DecimalStore extends NumberStore {
    constructor(conf, formStore){
        conf.decimal_value = true;
        super(conf, formStore)
    }
}

export {DecimalStore}
