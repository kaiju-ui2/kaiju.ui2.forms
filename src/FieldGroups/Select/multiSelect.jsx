import SelectStore from "FieldGroups/Select/simpleSelect";
import BaseValueStore from "FieldGroups/valueStore";
import {toJS} from "mobx";


class MultiValueStore extends BaseValueStore {
    constructor(value, conf, store) {
        super(value, conf, store);
        this.value = Array.isArray(value) ? value : [];
    }

    equal(valueStore) {
        let oldVal = Array.isArray(this.value) ? this.value : [];
        let newVal = Array.isArray(valueStore.value) ? valueStore.value : [];
        if (oldVal.length === 0 && newVal.length === 0) return true;
        if ((oldVal.length > 0 && newVal.length === 0) || (oldVal.length === 0 && newVal.length > 0)) return false;
        if (oldVal.length !== newVal.length) return false;

        let notMatch = oldVal.filter((oId, idx) => {
            return newVal[idx] !== oId
        });

        return notMatch.length === 0
    }
    get cleanData() {
        return toJS(this.value)
    }
}

export default class MultiSelectStore extends SelectStore {
    init() {
        this.ValueStore = MultiValueStore;
        this.conf.isMulti = true
    }
}

export {MultiValueStore}
