import SelectComponent from "FieldGroups/Select/component";
import {computed} from "mobx";
import FieldStore from "FieldGroups/store";
import BaseValueStore from "FieldGroups/valueStore";


export default class SelectStore extends FieldStore {
    Component = SelectComponent;

    @computed get isChanged() {
        if (!this.oldValue.value && !this.value.value){
            return false
        }

        return !this.oldValue.equal(this.value)
    }

    get cleanData() {
        return this.value.cleanData
    }

}

export {BaseValueStore}
