import React from "react";
import Label from "FieldGroups/Label";
import {uuid4} from "@kaiju.ui2/components/src/utils";

export default class BooleanComponent extends React.Component {
    constructor(props) {
        super(props)
        this.id = uuid4()
    }

    render() {
        return (
            <div className="form-group mw-500 relative kaiju-form__boolean" style={{position: "relative"}}>
                <div className=" pb-3">
                    <input type="checkbox" id={this.id} disabled={this.props.store.conf.disabled}
                           defaultChecked={this.props.store.value.value}
                           className="active-action ios8-switch ios8-switch-lg"
                           onChange={e => this.props.store.value.valueOnChange(e.target.checked)}/>
                    <Label  {...{id: this.id, props: this.props}}/>
                </div>
            </div>
        )
    }
}