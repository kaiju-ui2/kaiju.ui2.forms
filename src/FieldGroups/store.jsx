import {action, computed, observable} from "mobx";
import BaseValueStore from "FieldGroups/valueStore";
import {uuid4} from "@kaiju.ui2/components/src/utils";


class FieldStore {
    // 1. Определение behaviorComponent
    // 2. Хранение valueComponent'а
    // 3. Хранение всех данных поле
    // 4. Изменение value поля
    // 5. Вызывает сравнения у behavior - старое и новое значение


    @observable value;
    @observable show = true;
    @observable oldValue;
    ValueStore = BaseValueStore;
    Component; // указывать у children

    constructor(conf, formStore) {
        this.uuid = uuid4();
        this.conf = conf;
        this.conf.extraParams = formStore.extraParams;
        this.prefix = formStore.prefix;
        this.init();
        this.formStore = formStore;

        let cachedValue = this.getValueFromCache();

        this.value = this.setValue(cachedValue !== undefined ? cachedValue : this.conf.value || conf.default);
        this.oldValue = this.setValue(conf.value || conf.default);
    }

    reCreateValue() {
        let cachedValue = this.getValueFromCache();
        this.value = this.setValue(cachedValue !== undefined ? cachedValue : this.conf.value);
    }

    setSaveValue() {
        let cachedValue = this.getValueFromCache();
        this.oldValue = this.setValue(cachedValue !== undefined ? cachedValue : this.value.value);
    }

    saveValue(val) {
        // console.log("saveValue", val)
        // this.saveValueInCache(newVal)
    }

    getKey() {
        let per_channel = this.conf.per_channel || null;
        let per_locale = this.conf.per_locale || null;

        const channelId = this.formStore.conf.channelId;
        const locale = this.formStore.conf.locale;

        return `${per_channel ? channelId : null}.${per_locale ? locale : null}.${this.conf.key}`;
    }

    getValueFromCache() {
        this.checkCache();
        const key = this.getKey();

        if (Object.keys(this.formStore.cache.values).includes(key)) {
            return this.formStore.cache.values[key]
        }
    }

    checkCache() {
        if (!this.formStore.cache.values) {
            this.formStore.cache.values = {}
        }
    }

    saveValueInCache(value) {
        this.checkCache();
        const key = this.getKey()

        this.formStore.cache.values[key] = value;
    }

    clearCache() {
        this.saveValueInCache(undefined)
    }

    @action setValue(value) {
        return new this.ValueStore(value, this.conf, this)
    }

    @computed get isChanged() {
        return this.oldValue !== this.value
    }

    @action removeElement(index) {
        this.value = this.value.filter((el, idx) => {
            return idx !== index
        })
    }

    init() {
    }

    toggleShow() {
        this.show = !this.show
    }

    get cleanData() {
        let cachedValue = this.getValueFromCache();
        return this.value.cleanData
    }

    @action.bound setErrors(errors) {
        const key = this.getKey();
        this.formStore.cache.errors[key] = errors
    }

    @computed get errorMessage() {
        const key = this.getKey();
        return this.formStore.cache.errors[key];
    }

}

export default FieldStore;
