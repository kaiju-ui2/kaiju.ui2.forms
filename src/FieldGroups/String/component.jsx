import React from "react";
import BehaviorComponent from "Behavior/component";
import {observer} from "mobx-react";
import ErrorComponent from "Error";
import InputComponent from "@kaiju.ui2/components/src/Input";
import {uuid4} from "@kaiju.ui2/components/src/utils";


class SimpleStringComponent extends React.Component {
    valueRef;

    constructor(props) {
        super(props);

        this.value = props.value;

        this.conf = {
            ...props.conf,
            readonly: props.conf.is_system
        };
    }

    renderValueError = observer(() => {
        if (this.valueRef && this.value.valueError) {
            this.valueRef.classList.add("form-error")
        } else if (this.valueRef) {
            this.valueRef.classList.remove("form-error")
        }

        return (
            <div>
                {this.value.valueError &&
                <ErrorComponent id={uuid4()} value={this.value.valueError}/>
                }
            </div>
        )
    });

    setInputConf(ref, conf) {
        if (ref) {
            ref.readOnly = conf.readOnly;
            ref.className = `form-control m-input`
        }
    }

    render() {
        return (
            <div className="form-group mw-500 relative" style={{display: "flex", flexWrap: "wrap"}}>
                <div
                    className="mw-500 col-12 pr-0 pl-0"
                    ref={ref => this.valueRef = ref}
                    data-for={this.errorId}
                    data-tip=""
                >
                    <InputComponent
                        {...this.conf}
                        value={this.value.value}
                        callback={(value) => this.value.valueOnChange(value.value)}
                        getInputRef={ref => this.setInputConf(ref, this.conf)}
                    />
                    <this.renderValueError/>
                </div>

                {this.props.children}
            </div>
        )
    }
}


export default class StringComponent extends BehaviorComponent {
    field = SimpleStringComponent;
}

export {SimpleStringComponent};
