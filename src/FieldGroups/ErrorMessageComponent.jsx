import {observer} from "mobx-react";
import React from "react";
import {uuid4} from "@kaiju.ui2/components/src/utils";

@observer
export default class ErrorMessageComponent extends React.Component {
    render() {
        return (
            <div className={this.props.className || ""}>
                {this.props.store.errorMessage &&
                <React.Fragment>
                    {this.props.store.errorMessage.map((error) => {
                        return (
                            <span  key={uuid4()}>
                                <i className="icon-warn mr-3" style={{color: "red"}}/> {utils.getTranslation(error.code)}
                            </span>
                        )
                    })}
                </React.Fragment>
                }
            </div>
        )
    }
}
