import {action, observable} from "mobx";
import {FormStore} from "Form/FormStore";
import Axios from "axios";
import {notifySuccess} from "@kaiju.ui2/components/src/notifications";
import {deleteCache} from "Form/cache";

export default class WindowFormStore {
    @observable showModal = false;
    @observable isFetching = false;
    formStore;


    constructor(conf) {
        this.conf = conf;
        this.label = conf.label;
        this.saveRequestConf = (this.conf.requests || {}).save || this.conf.formStore.requests.save;
    }

    @action
    resetForm() {
        if (this.formStore) {
            deleteCache(this.formStore);
            delete this.formStore;
        }

        this.formStore = new FormStore(this.conf.formStore, this.conf.extraParams || {});
    }

    @action
    toggleWindow() {
        this.showModal = !this.showModal;

        if (this.showModal === true) {
            this.resetForm();
        }
    }

    setErrors(response) {
        let {error} = response.data;

        if (error && error.data.type === "ModelValidationException") {
            this.formStore.setErrors(error.data.fields)
        } else {
            utils.handleNotifyExceptions(response)
        }
    }


    saveCallback() {
        const isValid = true; // TODO:

        if (!isValid) {
            // TODO:
        }

        if (!this.formStore.changes) {
            return
        }

        if (!this.saveRequestConf) {
            this.conf.successCallback(this.formStore.cleanData);
            this.toggleWindow();
            return
        }

        this.isFetching = true;

        let params = {
            ...this.saveRequestConf.params,
            ...this.formStore.cleanData
        };

        Axios.post("/public/rpc", {
                method: this.saveRequestConf.method,
                params: params
            }
        ).then((response) => {
            const result = response.data.result;

            if (result) {
                notifySuccess(
                    this.conf.successTitle,
                    this.conf.successMessage,
                    this.conf.successDelay
                );
                this.conf.successCallback(result);
                this.toggleWindow();
            } else {
                this.setErrors(response)
            }

        })
            .catch((response) => utils.handleNotifyExceptions(response))
            .finally(() => this.isFetching = false)
    }
}