import React from "react";
import {inject, observer} from "mobx-react";
import ModalFragment, {TransparentModalFragment} from "@kaiju.ui2/components/src/Modal/ModalFragment";
import Form from "Form";
import {ButtonDefault, ButtonSuccess} from "@kaiju.ui2/components/src/Buttons";
import "./WindowForm.scss"
import "Form/Form.scss"

@inject("windowFormStore")
@observer
export default class WindowFormComponent extends React.Component {

    constructor(props) {
        super(props);

        this.cancelButtonConf = {
            className: "ml-3",
            label: utils.getTranslation("Button.cancel"),
            onClick: () => {
                this.props.windowFormStore.toggleWindow()
            }
        };

        this.successButtonConf = {
            label: utils.getTranslation("Button.confirm"),
            callback: () => {
                this.props.windowFormStore.saveCallback()
            },
        }
    }

    render() {
        const formStore = this.props.windowFormStore.formStore;

        return (
            <React.Fragment>
                {
                    this.props.windowFormStore.showModal &&
                    <ModalFragment show={true}
                                   cancelCallback={() => this.props.windowFormStore.toggleWindow()}>
                        <div className="m-portlet__body ml-auto mr-auto action-modal mw-500">
                            <div className="action-modal__notification row">
                                {
                                    formStore.actions.dataInitialized &&
                                    <React.Fragment>
                                        <div className="pl-3 col-12">
                                            <h1>{this.props.windowFormStore.label}</h1>
                                        </div>
                                        <div
                                            className={`pt-3 col-12 ${this.props.windowFormStore.isFetching ? 'disabled-window-form' : ''}`}>
                                            <Form
                                                className={this.props.className || ""}
                                                groupClassName={this.props.groupClassName || ""}
                                                key={formStore.key}
                                                store={formStore}
                                                groupFiltering/>
                                        </div>
                                        <div className="col-12">
                                            <ButtonSuccess
                                                {...this.successButtonConf}
                                                disabled={this.props.windowFormStore.isFetching}/>
                                            <ButtonDefault {...this.cancelButtonConf}/>
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </ModalFragment>
                }
            </React.Fragment>
        )
    }
}

@inject("windowFormStore")
@observer
class TransparentWindowFormComponent extends React.Component {
    ModalRef;

    constructor(props) {
        super(props);

        this.cancelButtonConf = {
            className: "ml-3",
            label: utils.getTranslation("Button.cancel"),
            onClick: () => {
                this.props.windowFormStore.toggleWindow()
            }
        };

        this.successButtonConf = {
            label: utils.getTranslation("Button.confirm"),
            callback: () => {
                this.props.windowFormStore.saveCallback();
            }
        };

        this.disableButtons = this.props.windowFormStore.conf.disableButtons === true; // for read only forms
    }

    componentDidMount() {
        document.addEventListener('mouseup', (e) => this.handleClickOutside(e));
    }

    handleClickOutside(event) {
        if (!this.props.windowFormStore.showModal) {
            return
        }

        if (this.ModalRef && !this.ModalRef.contains(event.target)) {
            this.props.windowFormStore.toggleWindow()
        }
    }

    render() {
        const formStore = this.props.windowFormStore.formStore;

        return (
            <React.Fragment>
                {
                    this.props.windowFormStore.showModal &&
                    <TransparentModalFragment show={true}
                                              contentClassName="window-from-content"
                                              cancelCallback={() => this.props.windowFormStore.toggleWindow()}>
                        <div className="m-portlet__body ml-auto mr-auto action-modal mw-500">

                            <div className="row action-modal-content p-5" ref={ref => this.ModalRef = ref}>
                                {
                                    formStore.actions.dataInitialized &&
                                    <React.Fragment>
                                        <div className="action-modal-cross"
                                             onClick={() => this.props.windowFormStore.toggleWindow()}>
                                            <i className="icon-cross pointer m--icon-font-size-lg4"/>
                                        </div>
                                        <div className="pl-3 col-12">
                                            <h1>{this.props.windowFormStore.label}</h1>
                                        </div>
                                        <div
                                            className={`pt-3 col-12 ${this.props.windowFormStore.isFetching ? 'disabled-window-form' : ''}`}>
                                            <Form key={formStore.key}
                                                  store={formStore}
                                                  groupFiltering/>
                                        </div>
                                        <div className="col-12">
                                            {
                                                !this.disableButtons &&
                                                <React.Fragment>
                                                    <ButtonSuccess
                                                        {...this.successButtonConf}
                                                        disabled={this.props.windowFormStore.isFetching}/>
                                                    <ButtonDefault {...this.cancelButtonConf}/>
                                                </React.Fragment>
                                            }
                                        </div>
                                    </React.Fragment>
                                }
                            </div>
                        </div>
                    </TransparentModalFragment>
                }
            </React.Fragment>
        )
    }
}


export {TransparentWindowFormComponent}