# README #

Kaiju forms - form workflow

### Components list
* Form
* WindowForm

### Data types
* String
* Number
* Select \ MultiSelect
* Boolean
* Datetime

## Form

### Options

| option      | type      | description                           | default | required  |
|------------ |-----------|---------------------------------------|---------|-----------|
|`labelKey` | string|  какой ключ считать лейблом,чтобы вывести его в хедер | "id" |   no  |
|`formStore` | json form conf   | конфиг формы| - |  yes   |
|`formStore.requests.load` | json conf   | method, params, errorCallback на получение формы| - |  yes   |
|`formStore.disableGroup` | bool   | убрать группировку полей по группам | false |  no   |
|`formStore.hasHeader` | bool   | вывести хедер с со строкой изменения | false |  no   |
|`requests.save` | json conf   | method, params, errorCallback на сохранение формы| - |  yes   |
|`changesCallback` | callback func   | передает состояние формы - были ли изменены данные (true, false) | - |  no   |


### Methods

`formHandleStore.label` - текущее значения лейбла
`formHandleStore.dataInitialized` - был ли выполненен Form.get
`formHandleStore.isFetching` - идёт ли запрос на бек в текущий момент времени

`WindowFormStore.toggleWindow()` - показать окно формы

### Examples
ВАЖНО: save request conf может быть в formStore.requests.save ИЛИ requests.save в корне.

```javascript
this.formHandleStore = new EditFormStore({
        label: "Обновление справочника организаций", (только у WindowFormStore)
        labelKey: "label",
        formStore: {
            disableGroup: true, // убрать группировку полей по группам
            key: utils.uuid4(),
            prefix: "attributeAdd.form", // префикс для кеша, если включен
            hasHeader: false, // header
            requests: {
                load: {
                    method: "Attribute.get", // метод отвечающий за отображение формы
                    params: {
                        // доп параметры
                        id: this.attributeKey
                    },
                    // при ошибке с бека
                    errorCallback: (responseData) => {
                        let errorCode = responseData.error ? responseData.error.code : '';
                        this.props.asyncErrorStore.setError(errorCode)
                    },
                },
                save: {
                    method: "Attribute.update", // метод отвечающий за обновление данных после валидации
                    params: {
                    // доп параметры
                        id: this.attributeKey
                    }
                }
            }
        },
        // колбэк на изменение формы
        changesCallback: (isChanged) => {
            editStore.setChanged(isChanged)
        }
    }
);
```

Пример полноценной страницы, где взаимодействует форма и хедер:
```javascript
import React from "react";
import DynamicHeader from "@kaiju.ui/components/src/Header/DynamicHeader";
import ROUTES from "RoutingNames";
import {inject, observer} from "mobx-react";
import asyncErrorDecorator from "@kaiju.ui/app/src/router/asyncErrorComponent";
import routerStore from "@kaiju.ui/app/src/stores/routerStore";
import "components/Loader/Loader.scss";
import EditFormStore from "@kaiju.ui/forms/src/Form/EditFormStore";
import Form from "@kaiju.ui/forms/src/Form";

@asyncErrorDecorator()
@inject("asyncErrorStore")
@inject("routerStore")
@inject("baseStorage")
@inject("userStore")
@observer
export default class DeclarationEditPage extends React.Component {
    constructor(props) {
        super(props);
        this.id = this.props.route.params["id"];

        this.declarationStore = new EditFormStore({
                labelKey: "number",
                formStore: {
                    disableGroup: true,
                    prefix: "Declaration.form",
                    hasHeader: false,
                    requests: {
                        load: {
                            method: "Declaration.get",
                            params: {
                                id: this.id,
                            },
                            errorCallback: (responseData) => utils.errorCallback({responseData, ...this.props})
                        }
                    }
                },
                requests: {
                    save: {
                        method: "Declaration.update",
                        params: {
                            id: this.id,
                        },
                        errorCallback: (responseData) => utils.errorCallback({responseData, ...this.props})
                    }
                },
            }
        );

        this.buttonConf = {
            label: utils.getTranslation("Button.save"),
            onClick: () => {
                this.formStore.save();
            }
        }
    }


    DynamicHeader = observer(() => {
        this.breadcrumbs = [
            {
                label: "Декларации таможни",
                path: ROUTES.declarationAll
            },
            {
                label: "Редактирование",
            },
        ];

        return (
            <DynamicHeader
                breadcrumbs={this.breadcrumbs}
                buttonConf={this.buttonConf}
                isChanged={this.mainFormStore.isChanged}
                isFetching={this.mainFormStore.isFetching}
                label={this.mainFormStore.label}
            />
        )
    });


    render() {
        return (
            <React.Fragment>
                <this.DynamicHeader/>
                {
                    this.mainFormStore.dataInitialized &&
                    <div className={this.mainFormStore.isFetching ? "disabled-form mt-5" : " mt-5"}>
                        <Form key={this.mainFormStore.formStore.key}
                              store={this.mainFormStore.formStore}
                              groupFiltering/>
                    </div>
                }
            </React.Fragment>
        )
    }
}
```

## Custom css ###

to change style of all forms:  
`kaiju-form__form` - css of form  

```scss
.any-form  {
   display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
}

.any-form  > .form-group{
  -webkit-box-flex: 0;
  -ms-flex: 0 0 33%;
  flex: 0 0 33%;
  max-width: 33%;
  padding-right: 10px;
  padding-bottom: 0!important;
  margin-bottom: 0!important;
}
```


Form style directly to component:  
`<Form 
groupClassName="group-style"
className="form-class-name"/>`

`.form-rows-2` - three columns form  
`.form-rows-3` - two columns form
