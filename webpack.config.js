const path = require('path');


module.exports = env => {

    return {
        entry: path.resolve(__dirname, "./src/index.jsx"),
        output: {
            path: path.resolve(__dirname, "dist"),
            filename: "bundle.js",
        },
        module: {
            rules: [
                {
                    test: /\.jsx$/,
                    exclude: /node_modules\/(?!@kaiju.ui\/).*/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: ["@babel/env", "@babel/react"]
                            }
                        }
                    ],
                },
                {
                    test: /\.css$/,
                    include: [
                        /node_modules/,
                        path.resolve(__dirname, './static/style.bundle.css')
                    ],
                    loaders: [
                        'style-loader',
                        'css-loader'
                    ]
                },
                {
                    test: /\.s[ac]ss$/i,
                    use: [
                        'style-loader',
                        'css-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                sassOptions: {
                                    includePaths: [
                                        path.resolve(__dirname, "node_modules/@kaiju.ui/components/src"),
                                        path.resolve(__dirname, "./src")
                                    ]
                                }
                            }
                        },
                    ],
                },
                {
                    test: /\.less$/i,
                    include: /node_modules/,
                    use: [
                        {loader: 'style-loader'},
                        {loader: 'css-loader'},
                        {loader: 'less-loader'}]
                },
                {
                    test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                    use: [
                        {
                            loader: 'file-loader'
                        }
                    ]
                }
            ]
        },
        resolve: {
            extensions: ['.js', '.jsx'],
        },
    };
};